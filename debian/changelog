librdf-helper-properties-perl (0.24-3) unstable; urgency=medium

  * Team upload.
  * Remove Makefile.old via debian/clean. (Closes: #1047405)

 -- gregor herrmann <gregoa@debian.org>  Thu, 07 Mar 2024 18:24:33 +0100

librdf-helper-properties-perl (0.24-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/*: update URLs from {search,www}.cpan.org to MetaCPAN.
  * debian/*: update GitHub URLs to use HTTPS.

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 17 Jun 2022 08:59:14 +0100

librdf-helper-properties-perl (0.24-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Fri, 08 Jan 2021 13:46:25 +0100

librdf-helper-properties-perl (0.24-1) unstable; urgency=medium

  [ upstream ]
  * New release.
    + Update to Moo.

  [ Jonas Smedegaard ]
  * Update watch file:
    + Bump to file format 4.
    + Watch MetaCPAN URL.
    + Mention gbp --uscan in usage comment.
    + Tighten version regex.
  * Modernize git-buildpackage config: Filter any .git* file.
  * Modernize Vcs-* fields:
    + Use https protocol.
    + Consistently use anonscm host.
    + Use cgit browser.
  * Declare compliance with Debian Policy 3.9.8.
  * Bump debhelper compatibility level to 9.
  * Drop CDBS get-orig-source target: Use gbp import-orig --uscan.
  * Update copyright info:
    + Use License-Grant and License-Reference fields.
      Thanks to Ben Finney.
    + Add alternate git source URL.
    + Extend coverage of Debian packaging.
  * Add lintian override regarding license in License-Reference field.
    See bug#786450.
  * Modernize CDBS use: Build-depend on licensecheck (not devscripts).
  * Update package relations:
    + (Build-)depend on libmoo-perl libtype-tiny-perl (not
      libany-moose-perl).
    + Build-depend on licensecheck (not devscripts).

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 02 Jan 2017 14:57:20 +0100

librdf-helper-properties-perl (0.22-2) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)

  [ Jonas Smedegaard ]
  * Add README.source emphasizing control.in file as *not* a show-stopper
    for contributions, referring to wiki page for details.

  [ Niko Tyni ]
  * Make the package autopkgtestable (again, see #845773)

 -- Niko Tyni <ntyni@debian.org>  Sun, 04 Dec 2016 22:53:25 +0200

librdf-helper-properties-perl (0.22-1) unstable; urgency=low

  * New upstream release.

  [ gregor herrmann ]
  * Remove debian/source/local-options: abort-on-upstream-changes and
    unapply-patches are default in dpkg-source since 1.16.1.

  [ Jonas Smedegaard ]
  * Bump debhelper compatibility level to 8.
  * Bump standards-version to 3.9.3.
  * Tidy rules file.
  * Update package relations:
    + Relax to build-depend unversioned on cdbs, debhelper and
      devscripts: Needed versions satisfied in stable, and oldstable no
      longer supported.
    + Build-depend and recommend libchi-perl.
    + (Build-)depend on libany-moose-perl.
    + Recommend (not depend on) libmoose-perl.
    + Tighten to (build-)depend versioned on
      libnamespace-autoclean-perl.
  * Update copyright file:
    + Adjust syntax to file format 1.0.
    + Update Files sections for included code copies of other projects.
    + Fix double-indent in Copyright fields as per Policy §5.6.13.
    + Fix use comment and license pseudo-sections to obey silly
      restrictions of copyright format 1.0.
    + Quote licenses in comments.
    + List upstream issue tracker as contact.
  * Use anonscm.debian.org (not git.debian.org) in Vcs-Browser URL.
  * Add Toby Inkster as upstream copyright holder.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 03 Nov 2012 02:08:28 +0100

librdf-helper-properties-perl (0.10-1) unstable; urgency=low

  * Initial packaging release.
    Closes: bug#626876.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 16 May 2011 05:09:24 +0200
